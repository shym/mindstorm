package com.jwbf.zpilegointerface;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import com.jwbf.zpilegointerface.datamodel.Robot;
import com.jwbf.zpilegointerface.dialog.RobotArrayAdapter;
import com.jwbf.zpilegointerface.observer.Observable;
import com.jwbf.zpilegointerface.service.BroadcastReceiverService;

import java.lang.reflect.Method;

/**
 * Created by jakub on 24.10.13.
 */

//TODO: na liście przycisk konfiguruj i graj

public class RobotListActivity extends AbstractRobotListActivity{



    protected void fillListWithNewData(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Robot[] robots = robotManager.getAllRobotsArray();
                System.out.println("All robots: " + robots.length);
                robotsArrayAdapter = new RobotArrayAdapter(RobotListActivity.this, R.layout.lvitem_robot, robots);
                robotListView.setAdapter(robotsArrayAdapter);
                robotsArrayAdapter.setEdit(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Robot robot = (Robot)view.getTag();
                        Intent intent = new Intent(RobotListActivity.this, CustomizeRobotActivity.class);
                        intent.putExtra(BundleConstants.DEVICE_ADDRESS, robot.getMacAdress());
                        startActivity(intent);
                    }
                });

                robotsArrayAdapter.setPlay(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        handleOnClickRobot((Robot) view.getTag());
                    }
                });
            }
        });
    }


    protected void handleOnClickRobot(Robot robot){

        String address = robot.getMacAdress();
        BluetoothDevice robotDevice = bluetoothManager.getRemoteDevice(address); 
        if(robotManager.getRobotByMac(address) == null){
            robotManager.saveRobot(robot);
        }

        if(robotDevice.getBondState() != BluetoothDevice.BOND_BONDED){
            try {
                if(createBond(robotDevice)){
                    progressDialog = ProgressDialog.show(RobotListActivity.this, "", "Bonding with device: " + robot.getName() + "...");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            bluetoothManager.cancelDiscovery();
            Intent intent = new Intent(RobotListActivity.this, GameActivity.class);
            intent.putExtra(BundleConstants.DEVICE_ADDRESS, robot.getMacAdress());
            startActivity(intent);
        }
    }

    private void handleBondStatusChanged(){
        if(progressDialog != null)
            progressDialog.dismiss();
        fillListWithNewData();
    }

    public boolean removeBond(BluetoothDevice btDevice)
            throws Exception
    {
        Class btClass = Class.forName("android.bluetooth.BluetoothDevice");
        Method removeBondMethod = btClass.getMethod("removeBond");
        Boolean returnValue = (Boolean) removeBondMethod.invoke(btDevice);
        return returnValue.booleanValue();
    }


    public boolean createBond(BluetoothDevice btDevice)
            throws Exception
    {
        Class class1 = Class.forName("android.bluetooth.BluetoothDevice");
        Method createBondMethod = class1.getMethod("createBond");
        Boolean returnValue = (Boolean) createBondMethod.invoke(btDevice);
        return returnValue.booleanValue();
    }

    @Override
    protected boolean shouldRefresh() {
        return true;
    }

    private void startService(){
        Intent intent = new Intent(RobotListActivity.this, BroadcastReceiverService.class);
        startService(intent);
    }

    /* Action bar and Menu Items setup */

    private void setupActionBar(){
        ActionBar actionBar = getActionBar();
        setTitle("Robster");
    }


    private void presentSettings(){
        Intent intent = new Intent(RobotListActivity.this, SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refreshList();
                return true;
            case R.id.action_settings:
                presentSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.robot_list_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_robot_list);
        super.onCreate(savedInstanceState);
        
        startService();
        setupActionBar();
    }

    protected void setupUi(){
        robotListView = (ListView) findViewById(R.id.lvRobotList);
    }

    @Override
    public void update(Observable o, int event) {
        switch (event){
            case RobotManagerEvents.EVENT_BONDING_FINISHED:
                handleBondStatusChanged();
                break;
            case RobotManagerEvents.EVENT_ADDED_NEARBY_ROBOT:
                fillListWithNewData();
            case RobotManagerEvents.EVENT_DISCOVERY_FINISHED:
                progressDialog.dismiss();
                break;
        }
    }
}
