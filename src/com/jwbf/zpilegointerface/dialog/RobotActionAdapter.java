package com.jwbf.zpilegointerface.dialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.jwbf.zpilegointerface.R;
import com.jwbf.zpilegointerface.RobotAction;
import com.jwbf.zpilegointerface.draganddrop.ArrowClickListener;
import com.jwbf.zpilegointerface.draganddrop.DragTouchListener;

/**
 * Created by jakub on 20.11.13.
 */
public class RobotActionAdapter extends BaseAdapter{

    private Context context;
    private RobotAction[] actions;

    private ArrowClickListener arrowClickListener;

    private Bitmap[] arrowBitmaps;

    public RobotActionAdapter(Context context, RobotAction[] actions){
        this.context = context;

        this.actions = actions;

        arrowBitmaps = new Bitmap[6];
        arrowBitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_left);
        arrowBitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_right);
        arrowBitmaps[2] = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_go);
        arrowBitmaps[3] = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_left_active);
        arrowBitmaps[4] = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_right_active);
        arrowBitmaps[5] = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_go_active);
    }

    public void setArrowClickListener(ArrowClickListener arrowClickListener) {
        this.arrowClickListener = arrowClickListener;
    }

    @Override
    public int getCount() {
        return actions.length;
    }

    @Override
    public Object getItem(int i) {
        return actions[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridCell;

        if(convertView == null) {
            gridCell = inflater.inflate(R.layout.grid_item_action, viewGroup, false);
        } else {
            gridCell = (View) convertView;
        }

        ImageView imgvActionArrow = (ImageView) gridCell.findViewById(R.id.imgvActionArrow);

        RobotAction action = actions[i];
        if (action.isActive()){
            imgvActionArrow.setImageBitmap(arrowBitmaps[action.getActionType()+3]);
        }
        else{
            imgvActionArrow.setImageBitmap(arrowBitmaps[action.getActionType()]);
        }

        DragTouchListener listener = new DragTouchListener(i, true);
        listener.setArrowClickListener(arrowClickListener);

        imgvActionArrow.setOnTouchListener(listener);

        gridCell.getHeight();

        return gridCell;
    }
}
