package com.jwbf.zpilegointerface.dialog;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.jwbf.zpilegointerface.R;
import com.jwbf.zpilegointerface.datamodel.Robot;
import com.jwbf.zpilegointerface.datamodel.RobotManager;

/**
 * Created by tetek on 11/5/13.
 */

public class RobotArrayAdapter extends ArrayAdapter<Robot> {

    private Robot[] elements;
    private Context context;
    private int layoutResourceId;


    public View.OnClickListener getPlay() {
        return play;
    }

    public void setPlay(View.OnClickListener play) {
        this.play = play;
    }

    public View.OnClickListener getEdit() {
        return edit;
    }

    public void setEdit(View.OnClickListener edit) {
        this.edit = edit;
    }

    private View.OnClickListener play;
    private View.OnClickListener edit;

    public RobotArrayAdapter(Context context, int layoutResourceId, Robot[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.elements = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RobotHolder holder = null;
        Robot robot = elements[position];
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new RobotHolder();
            holder.imgvRobotAvatar = (ImageView)row.findViewById(R.id.imgvRobotAvatar);
            holder.imgvRobotEdit = (ImageView)row.findViewById(R.id.imgvRobotEdit);
            holder.imgvRobotPlay = (ImageView)row.findViewById(R.id.imgvRobotPlay);
            holder.txtvRobotName = (TextView)row.findViewById(R.id.txtvRobotName);
            holder.txtvMacAdress = (TextView)row.findViewById(R.id.txtvMacAdress);
            holder.txtvIsPaired = (TextView)row.findViewById(R.id.txtvIsPaired);

            holder.imgvRobotEdit.setTag(robot);
            holder.imgvRobotPlay.setTag(robot);
            holder.imgvRobotPlay.setOnClickListener(getPlay());
            holder.imgvRobotEdit.setOnClickListener(getEdit());

            if(!RobotManager.getInstance(null).getNearbyRobots().contains(robot)){
                holder.imgvRobotPlay.setVisibility(View.GONE);
            } else {
                holder.imgvRobotPlay.setVisibility(View.VISIBLE);
            }


            row.setTag(holder);
        }
        else
        {
            holder = (RobotHolder)row.getTag();
        }


        holder.txtvMacAdress.setText(robot.getMacAdress());
        holder.txtvRobotName.setText(robot.getName());
        if(robot.isBonded()){
            holder.txtvIsPaired.setText("Połączony");
        } else {
            holder.txtvIsPaired.setText("Niepołączony");
        }
        holder.imgvRobotAvatar.setImageBitmap(robot.getAvatar(context));

        return row;
    }



    class RobotHolder{
        ImageView imgvRobotAvatar;
        ImageView imgvRobotPlay;
        ImageView imgvRobotEdit;
        TextView txtvRobotName;
        TextView txtvMacAdress;
        TextView txtvIsPaired;
    }
}