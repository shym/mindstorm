package com.jwbf.zpilegointerface.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

import com.jwbf.zpilegointerface.RobotManagerEvents;
import com.jwbf.zpilegointerface.bluetooth.BluetoothManager;
import com.jwbf.zpilegointerface.datamodel.Robot;
import com.jwbf.zpilegointerface.datamodel.RobotManager;
import com.lego.minddroid.BTCommunicator;

/**
 * Created by jakub on 07.11.13.
 */
public class BroadcastReceiverService extends Service{

    private RobotManager robotManager;
    private BluetoothManager bluetoothManager;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                if(!device.getAddress().startsWith(BTCommunicator.OUI_LEGO)){
                    return;
                }


                Robot newNearbyRobot = new Robot();
                newNearbyRobot.setName(device.getName());
                newNearbyRobot.setMacAdress(device.getAddress());

                if(robotManager == null){
                    robotManager = RobotManager.getInstance(getApplicationContext());
                }

                robotManager.addNearbyRobot(newNearbyRobot);

            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                Log.d("DEVICE", "Discovery finished !");
                robotManager.notifyObservers(RobotManagerEvents.EVENT_DISCOVERY_FINISHED);
//                fillDevicesListWithAllRobots();
            } else if(BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                int prevBondState = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, -1);
                int bondState = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, -1);

                if (prevBondState == BluetoothDevice.BOND_BONDING)
                {
                    // check for both BONDED and NONE here because in some error cases the bonding fails and we need to fail gracefully.
                    if (bondState == BluetoothDevice.BOND_BONDED /*|| bondState == BluetoothDevice.BOND_NONE*/)
                    {
                        robotManager.notifyObservers(RobotManagerEvents.EVENT_BONDING_FINISHED);
                    }
                }

            }


        }
    };

    private void setupReceiver(){
        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, filter);

        //Register for broadcasts when bond state is changed
        filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        this.registerReceiver(mReceiver, filter);
    }

    private void setupBluetooth(){
        bluetoothManager = BluetoothManager.getInstance();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        robotManager = RobotManager.getInstance(getApplicationContext());
        setupReceiver();
        setupBluetooth();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
