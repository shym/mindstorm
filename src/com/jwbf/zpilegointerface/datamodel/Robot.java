package com.jwbf.zpilegointerface.datamodel;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.jwbf.zpilegointerface.R;
import com.jwbf.zpilegointerface.bluetooth.BluetoothManager;

/**
 * Created by jakub on 24.10.13.
 */
public class Robot {

    private String name;
    private String macAdress;
    private String avatarPath = "";
    private boolean isNearby;

    private long leftDelay = 1250;
    private long rightDelay = 1250;
    private long goDelay = 1250;

    private BluetoothManager bluetoothManager;

    public Robot(){
        bluetoothManager = BluetoothManager.getInstance();
    }

    public boolean isBonded() {
        BluetoothDevice robotDevice = bluetoothManager.getRemoteDevice(macAdress);

        return (robotDevice.getBondState() == BluetoothDevice.BOND_BONDED);
    }

    public boolean isNearby() {
        return isNearby;
    }

    public void setNearby(boolean isNearby) {
        this.isNearby = isNearby;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMacAdress() {
        return macAdress;
    }

    public void setMacAdress(String macAdress) {
        this.macAdress = macAdress;
    }

    public long getLeftDelay() {
        return leftDelay;
    }

    public void setLeftDelay(long leftDelay) {
        this.leftDelay = leftDelay;
    }

    public long getRightDelay() {
        return rightDelay;
    }

    public void setRightDelay(long rightDelay) {
        this.rightDelay = rightDelay;
    }

    public long getGoDelay() {
        return goDelay;
    }

    public void setGoDelay(long goDelay) {
        this.goDelay = goDelay;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public Bitmap getAvatar(Context context){
        if (avatarPath != null){
            if (avatarPath.length() > 3)
                return BitmapFactory.decodeFile(avatarPath);
        }
        Drawable drawable = context.getResources().getDrawable(R.drawable.ic_launcher);
        Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
        return bitmap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Robot robot = (Robot) o;

        if (macAdress != null ? !macAdress.equals(robot.macAdress) : robot.macAdress != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return macAdress != null ? macAdress.hashCode() : 0;
    }
}
