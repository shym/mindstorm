package com.jwbf.zpilegointerface.datamodel;

import android.content.Context;
import android.view.DragEvent;
import com.jwbf.zpilegointerface.RobotManagerEvents;
import com.jwbf.zpilegointerface.db.DbHelper;
import com.jwbf.zpilegointerface.observer.Observable;
import com.jwbf.zpilegointerface.observer.Observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jakub on 24.10.13.
 */
public class RobotManager implements Observable{

    private static RobotManager robotManager;
    private DbHelper dbHelper;

    private ArrayList<Robot> nearbyRobots = new ArrayList<Robot>();
    private ArrayList<Observer> observers = new ArrayList<Observer>();

    private int arrowTypeOnDrop = -1;
    private int arrowIdToRemove = -1;

    private DragEvent lastDragEvent;

    /* Constructors */
    private RobotManager(Context context) {
        dbHelper = new DbHelper(context);
        dbHelper.openDataBase();
    }

    public static RobotManager getInstance(Context context){
        if(robotManager == null ){
            robotManager = new RobotManager(context);
        }
        return robotManager;
    }
    public static RobotManager getInstance(){
        if(robotManager == null ){
            robotManager = new RobotManager(null);
        }
        return robotManager;
    }
    public void notifyObservers(int event){
        for(Observer o : observers){
            o.update(this, event);
        }
    }

    public void addNearbyRobot(Robot robot){
        System.out.println("Add Nearby Robot: " + robot.getName());
        if(!nearbyRobots.contains(robot)){
            robot.setNearby(true);
            nearbyRobots.add(robot);
            notifyObservers(RobotManagerEvents.EVENT_ADDED_NEARBY_ROBOT);
        }
    }



    public Robot getRobotWithMacAdress(String macAdress){
        return getRobotByMac(macAdress);
    }

    public int getArrowTypeOnDrop() {
        return arrowTypeOnDrop;
    }

    public void setArrowTypeOnDrop(int arrowTypeOnDrop) {
        this.arrowTypeOnDrop = arrowTypeOnDrop;
    }

    /* Getting lists of robots */

    /**
     * nearbyRobots is being fillen from BroadCastReceiver. It's a time-defined list of robots
     * available via bluetooth.
     * @return Clean list of robots in nearby (have nothing to do with saved robots).
     */
    public ArrayList<Robot> getNearbyRobots(){
        return nearbyRobots;
    }

    /**
     * Uses DataBase helper to get all saved Robots.
     * @return List of robots saved in database.
     */
    private List<Robot> getSavedRobots(){
        List<Robot> robots = dbHelper.getAllRobots();

        return robots;
    }

    /**
     * Active Robot is a robot which is both in database and in nearby.
     * @return Saved robots which are in also on the bluetooth at the moment of calling.
     */
    public ArrayList<Robot> getActiveRobots(){
        ArrayList<Robot> activeRobots = new ArrayList<Robot>();
        List<Robot> savedRobots = getSavedRobots();

        System.out.println("Saved Robots: " + savedRobots.size() + "\nNearby Robots: " + nearbyRobots.size());

        for(Robot robot : savedRobots){
            if(nearbyRobots.contains(robot)){
                activeRobots.add(robot);
            }
        }

        return activeRobots;
    }


    /**
     * Merges robots from nearby and from database.
     * @return List of all robots, both in nearby and saved ones which are not nearby.
     */
    public ArrayList<Robot> getAllRobots(){
        ArrayList<Robot> allRobots = new ArrayList<Robot>();

        for(Robot robot : getSavedRobots()){
            allRobots.add(robot);
        }

        for(Robot robot : nearbyRobots){
            if(!allRobots.contains(robot)){
                allRobots.add(robot);
            }
        }

        return allRobots;
    }

    public Robot[] getAllRobotsArray(){
        ArrayList<Robot> allRobots = getAllRobots();
        Robot[] robotsArray = new Robot[allRobots.size()];

        for(int i=0; i<allRobots.size(); i++){
            robotsArray[i] = allRobots.get(i);
        }

        return robotsArray;
    }

    public Robot[] getActiveRobotsArray(){
        ArrayList<Robot> activeRobots = getActiveRobots();
        Robot[] robotsArray = new Robot[activeRobots.size()];

        for(int i=0; i<activeRobots.size(); i++){
            robotsArray[i] = activeRobots.get(i);
        }

        return robotsArray;
    }

    public int getArrowIdToRemove() {
        return arrowIdToRemove;
    }

    public void setArrowIdToRemove(int arrowIdToRemove) {
        this.arrowIdToRemove = arrowIdToRemove;
    }

    public DragEvent getLastDragEvent() {
        return lastDragEvent;
    }

    public void setLastDragEvent(DragEvent lastDragEvent) {
        this.lastDragEvent = lastDragEvent;
    }

    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void delObserver(Observer o) {
        observers.remove(o);
    }

    public void saveRobot(Robot robot){
        dbHelper.updateRobot(robot);
    }

    public Robot getRobotByMac(String mac){
        return dbHelper.getRobotByMac(mac);
    }
}
