package com.jwbf.zpilegointerface.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

/**
 * Created by jakub on 17.10.13.
 */
public class BluetoothManager {

    private static BluetoothManager bluetoothManager;

    // Member fields
    private BluetoothAdapter mBtAdapter;

    private BluetoothManager() {
        // Get the local Bluetooth adapter
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public static BluetoothManager getInstance(){
        if(bluetoothManager == null){
            bluetoothManager = new BluetoothManager();
        }

        return bluetoothManager;
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    public void doDiscovery() {

        // If we're already discovering, stop it
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }

        // Request discover from BluetoothAdapter
        mBtAdapter.startDiscovery();
    }

    public void cancelDiscovery() {
        mBtAdapter.cancelDiscovery();
    }

    public BluetoothDevice getRemoteDevice(String address){
        return mBtAdapter.getRemoteDevice(address);
    }


}
