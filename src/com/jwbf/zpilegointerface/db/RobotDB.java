package com.jwbf.zpilegointerface.db;

/**
 * Created by jakub on 05.11.13.
 */
public class RobotDB {

    public final class RobotTable{

        public final class Column{
            public static final String ID = "_id";
            public static final String MAC = "macaddr";
            public static final String NAME = "name";
            public static final String AVATAR = "avatarpath";
            public static final String LEFT = "left";
            public static final String RIGHT = "right";
            public static final String GO = "go";
        }

        public final class ColumndID{
            public static final int ID = 0;
            public static final int MAC = 1;
            public static final int NAME = 2;
            public static final int AVATAR = 3;
            public static final int LEFT = 4;
            public static final int RIGHT = 5;
            public static final int GO = 6;
        }
    }

}
