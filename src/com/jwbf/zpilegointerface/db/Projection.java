package com.jwbf.zpilegointerface.db;

/**
 * Created by jakub on 05.11.13.
 */
public class Projection {

    public static final class RobotInfo{
        public static final String[] Columns = {
                RobotDB.RobotTable.Column.ID,
                RobotDB.RobotTable.Column.MAC,
                RobotDB.RobotTable.Column.NAME,
                RobotDB.RobotTable.Column.AVATAR
        };

        public static class Column {
            public static final String ID = RobotDB.RobotTable.Column.ID;
            public static final String MAC = RobotDB.RobotTable.Column.MAC;
            public static final String NAME = RobotDB.RobotTable.Column.NAME;
            public static final String AVATAR = RobotDB.RobotTable.Column.AVATAR;
        }

        public static class ColumnID{
            public static final int ID = 0;
            public static final int MAC = 1;
            public static final int NAME = 2;
            public static final int AVATAR = 3;
        }
    }

}
