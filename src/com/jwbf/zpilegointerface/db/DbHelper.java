package com.jwbf.zpilegointerface.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.jwbf.zpilegointerface.datamodel.Robot;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by jakub on 05.11.13.
 */

public class DbHelper extends SQLiteOpenHelper{
    private static final String DB_NAME = "robot.sqlite";
    private static final String DB_ROBOT_TABLE = "Robot";

    private static final String DATABASE_CREATE = "create table "
            + DB_ROBOT_TABLE + "(" +
            RobotDB.RobotTable.Column.ID +      " integer primary key autoincrement, " +
            RobotDB.RobotTable.Column.MAC +     " text not null, " +
            RobotDB.RobotTable.Column.NAME +    " text not null, " +
            RobotDB.RobotTable.Column.AVATAR +  " text not null, " +
            RobotDB.RobotTable.Column.LEFT +    " integer not null default 1250, " +
            RobotDB.RobotTable.Column.RIGHT +   " integer not null default 1250, " +
            RobotDB.RobotTable.Column.GO +      " integer not null default 1250);";


    private Context context;
    private SQLiteDatabase sqliteDB;

    public DbHelper(Context context){
        super(context, DB_NAME, null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Log.w(DbHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DB_ROBOT_TABLE);
        onCreate(sqLiteDatabase);
    }

    public void openDataBase() throws SQLiteException{
        if(sqliteDB != null){
            sqliteDB.close();
        }

        sqliteDB = getWritableDatabase();
    }

    public long addRobot(Robot robot){

        ContentValues values = new ContentValues();

        values.put(RobotDB.RobotTable.Column.MAC, robot.getMacAdress());
        values.put(RobotDB.RobotTable.Column.NAME, robot.getName());
        values.put(RobotDB.RobotTable.Column.AVATAR, robot.getAvatarPath());
        values.put(RobotDB.RobotTable.Column.LEFT, robot.getLeftDelay());
        values.put(RobotDB.RobotTable.Column.RIGHT, robot.getRightDelay());
        values.put(RobotDB.RobotTable.Column.GO, robot.getGoDelay());

        Log.d("VALUES", "" + values);

        long id = sqliteDB.insert(DB_ROBOT_TABLE, null, values);

        System.out.println("DODALEM KURWA ROBOTA ID: " + id);
        return id;
    }

    public void updateRobot(Robot robot){

        if(!isRobotInDb(robot.getMacAdress())){
            addRobot(robot);
        } else {
            ContentValues values = new ContentValues();

            values.put(RobotDB.RobotTable.Column.NAME, robot.getName());
            values.put(RobotDB.RobotTable.Column.AVATAR, robot.getAvatarPath());
            values.put(RobotDB.RobotTable.Column.LEFT, robot.getLeftDelay());
            values.put(RobotDB.RobotTable.Column.RIGHT, robot.getRightDelay());
            values.put(RobotDB.RobotTable.Column.GO, robot.getGoDelay());

            sqliteDB.update(DB_ROBOT_TABLE, values, RobotDB.RobotTable.Column.MAC + " = '" + robot.getMacAdress() + "'", null);
        }
    }

    public boolean isRobotInDb(String mac){
        boolean result = false;
        Cursor cursor = sqliteDB.query(DB_ROBOT_TABLE, null, RobotDB.RobotTable.Column.MAC + " = '" + mac + "'", null, null, null, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                do{
                    String macc = cursor.getString(RobotDB.RobotTable.ColumndID.MAC);
                    String name = cursor.getString(RobotDB.RobotTable.ColumndID.NAME);

                    System.out.println("MAM ROBOTA O MACU : " + macc + " i nazwie: " + name);
                    if(macc.equals(mac)){
                        result = true;
                    }
                } while (cursor.moveToNext());
            }
        }

        return result;
    }

    public Robot getRobotByMac(String mac){
        Robot robot = null;

        Cursor cursor = sqliteDB.query(DB_ROBOT_TABLE, null, RobotDB.RobotTable.Column.MAC + " ='" + mac + "'", null, null, null, null);

        if(cursor != null){
            if(cursor.moveToFirst()){
                do{

                    String name = cursor.getString(RobotDB.RobotTable.ColumndID.NAME);
                    String avatarPath = cursor.getString(RobotDB.RobotTable.ColumndID.AVATAR);
                    long leftDelay = cursor.getLong(RobotDB.RobotTable.ColumndID.LEFT);
                    long rightDelay = cursor.getLong(RobotDB.RobotTable.ColumndID.RIGHT);
                    long goDelay = cursor.getLong(RobotDB.RobotTable.ColumndID.GO);
                    robot = new Robot();
                    robot.setMacAdress(mac);
                    robot.setName(name);
                    robot.setAvatarPath(avatarPath);
                    robot.setRightDelay(rightDelay);
                    robot.setLeftDelay(leftDelay);
                    robot.setGoDelay(goDelay);

                } while (cursor.moveToNext());
            }
        }

        return robot;
    }

    public List<Robot> getAllRobots(){
        List<Robot> robots = new LinkedList<Robot>();

        Cursor c = sqliteDB.query(DB_ROBOT_TABLE, null, null, null, null, null, null);

        if(c != null){
            if(c.moveToFirst()){
                do{
                    String mac = c.getString(RobotDB.RobotTable.ColumndID.MAC);
                    String name = c.getString(RobotDB.RobotTable.ColumndID.NAME);
                    String avatarPath = c.getString(RobotDB.RobotTable.ColumndID.AVATAR);
                    long leftDelay = c.getLong(RobotDB.RobotTable.ColumndID.LEFT);
                    long rightDelay = c.getLong(RobotDB.RobotTable.ColumndID.RIGHT);
                    long goDelay = c.getLong(RobotDB.RobotTable.ColumndID.GO);
                    Robot robotToAdd = new Robot();
                    robotToAdd.setMacAdress(mac);
                    robotToAdd.setName(name);
                    robotToAdd.setAvatarPath(avatarPath);
                    robotToAdd.setRightDelay(rightDelay);
                    robotToAdd.setLeftDelay(leftDelay);
                    robotToAdd.setGoDelay(goDelay);
                    robots.add(robotToAdd);
                } while (c.moveToNext());
            }
        }

        return robots;
    }

    @Override
    public synchronized void close() {
        if (sqliteDB != null)
            sqliteDB.close();

        super.close();
    }

}
