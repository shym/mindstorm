package com.jwbf.zpilegointerface;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jwbf.zpilegointerface.bluetooth.BluetoothManager;
import com.jwbf.zpilegointerface.datamodel.Robot;
import com.jwbf.zpilegointerface.datamodel.RobotManager;
import com.jwbf.zpilegointerface.dialog.RobotArrayAdapter;
import com.jwbf.zpilegointerface.observer.Observable;
import com.jwbf.zpilegointerface.observer.Observer;

/**
 * Created by tetek on 11/12/13.
 */

public abstract class AbstractRobotListActivity extends Activity implements Observer{



    /**
     * Array Adapter handling robots list from RobotManager.
     */
    protected RobotArrayAdapter robotsArrayAdapter;

    /**
     * Progress HUD when discovering devices.
     */
    protected ProgressDialog progressDialog;
    /**
     * Reference for Robot Manager singleton.
     */
    protected RobotManager robotManager;
    /**
     * Reference for Bluetooth Manager singleton.
     */
    protected BluetoothManager bluetoothManager;

    /**
     * List View Component displaying  list of devices from robotsArrayAdapter.
     * */
    protected ListView robotListView;

    protected abstract void handleOnClickRobot(Robot robot);
    protected abstract void fillListWithNewData();
    protected abstract boolean shouldRefresh();
    private void setupListeners(){
        robotListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Robot robot = robotsArrayAdapter.getItem(i);
                handleOnClickRobot(robot);
            }
        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //first make references
        robotManager = RobotManager.getInstance(getApplicationContext());
        bluetoothManager = BluetoothManager.getInstance();

        setupUi();
        setupListeners();
        if (shouldRefresh()){
            refreshList();
        }


    }


    protected void setupUi(){
        robotListView = (ListView) findViewById(R.id.lvRobotList);
    }

    protected void refreshList(){
        bluetoothManager.doDiscovery();
        progressDialog = ProgressDialog.show(AbstractRobotListActivity.this, "", "Wyszukiwanie robotów..");
    }

    @Override
    protected void onResume() {
        super.onResume();
        fillListWithNewData();
        robotManager.addObserver(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        robotManager.delObserver(this);
    }

    @Override
    public void update(Observable o, int event) {
        switch (event){


            case RobotManagerEvents.EVENT_ADDED_NEARBY_ROBOT:
                fillListWithNewData();
            case RobotManagerEvents.EVENT_DISCOVERY_FINISHED:
                if(progressDialog != null)
                    progressDialog.dismiss();
                break;
            case RobotManagerEvents.EVENT_BONDING_FINISHED:
//                handleBondStatusChanged();
                break;
        }
    }
}
