package com.jwbf.zpilegointerface.lego;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import com.jwbf.zpilegointerface.BundleConstants;
import com.jwbf.zpilegointerface.RobotAction;
import com.lego.minddroid.R;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by tetek on 11/28/13.
 */
public class Communicator implements BTConnectable {

    private boolean connected;


    public  CommunicatorListener listener;
    public BTCommunicator myBTCommunicator;
    private Handler btcHandler;
    public boolean isPlaying;
    public boolean stop;

    @Override
    public boolean isPairing() {
        return false;
    }


    /**
     * Creates and starts the a thread for communication via bluetooth to the NXT robot.
     * @param mac_address The MAC address of the NXT robot.
     */
    public void startBTCommunicator(String mac_address, Resources r,Context ctx) {
        System.out.println("MacAdress: " + mac_address);
        connected = false;
        if (listener != null){
            listener.presentMessage("Trwa łączenie z robotem.");
        }

        if (myBTCommunicator != null) {
            try {
                myBTCommunicator.destroyNXTconnection();
            }
            catch (IOException e) { }
        }
        createBTCommunicator(r,ctx);
        myBTCommunicator.setMACAddress(mac_address);
        myBTCommunicator.start();
    }

    /**
     * Creates a new object for communication to the NXT robot via bluetooth and fetches the corresponding handler.
     */
    private void createBTCommunicator(Resources r,Context ctx) {
        // interestingly BT adapter needs to be obtained by the UI thread - so we pass it in in the constructor
        myBTCommunicator = new BTCommunicator(this, myHandler, BluetoothAdapter.getDefaultAdapter(), r,ctx);
        btcHandler = myBTCommunicator.getHandler();
    }

    /**
     * Sends a message for disconnecting to the communcation thread.
     */
    public void destroyBTCommunicator() {

        if (myBTCommunicator != null) {
            sendBTCmessage(BTCommunicator.NO_DELAY, BTCommunicator.DISCONNECT, 0, 0);
            myBTCommunicator = null;
        }

        connected = false;
    }

    /**
     * Sends the message via the BTCommuncator to the robot.
     * @param delay time to wait before sending the message.
     * @param message the message type (as defined in BTCommucator)
     * @param value1 first parameter
     * @param value2 second parameter
     */
    void sendBTCmessage(int delay, int message, int value1, int value2) {
        Bundle myBundle = new Bundle();
        myBundle.putInt("message", message);
        myBundle.putInt("value1", value1);
        myBundle.putInt("value2", value2);
        Message myMessage = myHandler.obtainMessage();
        myMessage.setData(myBundle);

        if (delay == 0)
            btcHandler.sendMessage(myMessage);

        else
            btcHandler.sendMessageDelayed(myMessage, delay);
    }

    /**
     * Sends the message via the BTCommuncator to the robot.
     * @param delay time to wait before sending the message.
     * @param message the message type (as defined in BTCommucator)
     * @param String a String parameter
     */
    void sendBTCmessage(int delay, int message, String name) {
        Bundle myBundle = new Bundle();
        myBundle.putInt("message", message);
        myBundle.putString("name", name);
        Message myMessage = myHandler.obtainMessage();
        myMessage.setData(myBundle);

        if (delay == 0)
            btcHandler.sendMessage(myMessage);
        else
            btcHandler.sendMessageDelayed(myMessage, delay);
    }


    /**
     * Receive messages from the BTCommunicator
     */
    final Handler myHandler = new Handler() {
        @Override
        public void handleMessage(Message myMessage) {
            int MESSAGE_CODE = myMessage.getData().getInt("message");
            System.out.println( MESSAGE_CODE + "message code");

            switch (myMessage.getData().getInt("message")) {
                case BTCommunicator.DISPLAY_TOAST:
                    break;
                case BTCommunicator.STATE_CONNECTED:
                    System.out.println("STATE CONNECTED !");
                    if (listener != null){
                        listener.hideMessage();
                    }
                    connected = true;
                    sendBTCmessage(BTCommunicator.NO_DELAY, BTCommunicator.GET_FIRMWARE_VERSION, 0, 0);

                    break;

                case BTCommunicator.STATE_CONNECTERROR_PAIRING:
                    destroyBTCommunicator();
                    if (listener != null){
                        listener.hideMessage();
                    }
                    break;

                case BTCommunicator.STATE_CONNECTERROR:
                case BTCommunicator.STATE_RECEIVEERROR:
                    destroyBTCommunicator();
                    break;
                case BTCommunicator.STATE_SENDERROR:
                    if (listener != null){
                        listener.hideMessage();
                    }
                    destroyBTCommunicator();
                    break;

            }
        }
    };

    /**
     *  Sends messages to the current robot. Stops when robot is touching a wall.
      * @param actions is an array list of RobotAction, which has neccesary motor info.
     */
    public void playActions(final ArrayList<RobotAction> actions){
        new Thread(){
            @Override
            public void run() {
                super.run();
                myBTCommunicator.isTouching = false;
                try {
                    myBTCommunicator.sendMessage(LCPMessage.getSetTouchMessage());
                    receive();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                isPlaying = true;
                stop = false;
                int i = Math.max(0,actions.size()-1);

                for (; i > 0; i--){
                    RobotAction a = actions.get(i);
                    if (a.isActive()){
                       break;
                    }
                }
                for (int j = i; j < actions.size(); j++){
                    RobotAction action = actions.get(j);
                    if (myBTCommunicator.isTouching || stop){
                        break;
                    }
                    action.setActive(true);
                    playAction(action);
                    if (listener != null){
                        listener.refreshGridView();
                    }


                    try {
                        Thread.sleep(200+Math.max(action.getLeftDelay(), action.getRightDelay()));
                        Log.d("e","hehe");

                        action.setActive(stop || myBTCommunicator.isTouching);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
                isPlaying = false;
                if (listener != null){
                    listener.refreshGridView();
                }
            }

        }.start();

    }

    /**
     * Sends messages on a background thread asking for sensor values.
     */
    private void receive(){
        new Thread(){
            @Override
            public void run() {
                super.run();

                while (myBTCommunicator != null && !myBTCommunicator.isTouching && !stop){

                    try {

                        myBTCommunicator.sendMessage(LCPMessage.getInputValuesMessage());

                    } catch (IOException e) {
                        e.printStackTrace();
                        break;
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                }
            }
        }.start();

    }

    /**
     * Send RobotAction to currently connected robot.
     */
    public void playAction(RobotAction action){
        sendBTCmessage(com.lego.minddroid.BTCommunicator.NO_DELAY, action.getLeftMotorID() ,action.getLeftMotorPower(), 0);
        sendBTCmessage(com.lego.minddroid.BTCommunicator.NO_DELAY, action.getRightMotorID(),action.getRightMotorPower(), 0);
        sendBTCmessage(action.getLeftDelay(), action.getLeftMotorID(), 0, 0);
        sendBTCmessage(action.getRightDelay(), action.getRightMotorID(), 0, 0);
    }
}
