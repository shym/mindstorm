package com.jwbf.zpilegointerface.lego;

import com.jwbf.zpilegointerface.RobotAction;

/**
 * Created by tetek on 11/28/13.
 */
public interface CommunicatorListener {

    public void refreshGridView();
    public void presentMessage(String message);
    public void hideMessage();

}
