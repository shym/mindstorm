package com.jwbf.zpilegointerface;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.*;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.jwbf.zpilegointerface.datamodel.Robot;
import com.jwbf.zpilegointerface.datamodel.RobotManager;
import com.jwbf.zpilegointerface.lego.Communicator;
import com.jwbf.zpilegointerface.lego.CommunicatorListener;

/**
 * Created by jakub on 24.10.13.
 */


public class CustomizeRobotActivity extends Activity implements CommunicatorListener{

    private ImageView avatarImageView;
    private EditText robotNameEditText;
    private TextView macAddressTextView;

    private ImageView imgvIncrementLeft;
    private ImageView imgvIncrementRight;
    private ImageView imgvIncrementGo;
    private ImageView imgvDecrementLeft;
    private ImageView imgvDecrementRight;
    private ImageView imgvDecrementGo;

    private ImageView imgvTestLeft;
    private ImageView imgvTestRight;
    private ImageView imgvTestGo;
    private ImageView imgvSave;

    private TextView txtvLeftDelay;
    private TextView txtvRightDelay;
    private TextView txtvGoDelay;


    private Robot robot;
    private Communicator robotCommunicator;
    private ProgressDialog connectingProgressDialog;

    private int type = -1;

    private static final int SELECT_PHOTO = 100;
    private static final int INCREMENT_DECREMENT_FACTOR = 10;

    //----- UI ------
    protected void setupUI(){
        avatarImageView =  (ImageView)findViewById(R.id.avatarImageView);
        robotNameEditText = (EditText)findViewById(R.id.robotNameEditText);
        macAddressTextView = (TextView)findViewById(R.id.macAddressTextView);

        imgvIncrementGo = (ImageView) findViewById(R.id.imgvIncrementGo);
        imgvIncrementLeft = (ImageView) findViewById(R.id.imgvIncrementLeft);
        imgvIncrementRight = (ImageView) findViewById(R.id.imgvIncrementRight);
        imgvDecrementGo = (ImageView) findViewById(R.id.imgvDecrementGo);
        imgvDecrementLeft = (ImageView) findViewById(R.id.imgvDecrementLeft);
        imgvDecrementRight = (ImageView) findViewById(R.id.imgvDecrementRight);

        imgvTestLeft = (ImageView) findViewById(R.id.imgvTestLeft);
        imgvTestRight = (ImageView) findViewById(R.id.imgvTestRight);
        imgvTestGo = (ImageView) findViewById(R.id.imgvTestGo);

        txtvLeftDelay = (TextView) findViewById(R.id.txtvLeftDelay);
        txtvRightDelay = (TextView) findViewById(R.id.txtRightoDelay);
        txtvGoDelay = (TextView) findViewById(R.id.txtvGoDelay);
        imgvSave = (ImageView) findViewById(R.id.imgvCustomSave);
    }

    protected void setupListeners(){
        avatarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, SELECT_PHOTO);
            }
        }
        );

        imgvTestLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                robotCommunicator.playAction(RobotAction.actionLeft(robot));
            }
        });

        imgvTestRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                robotCommunicator.playAction(RobotAction.actionRight(robot));
            }
        });

        imgvTestGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                robotCommunicator.playAction(RobotAction.actionGo(robot));
            }
        });

        imgvIncrementGo.setOnTouchListener(touchListener);
        imgvIncrementRight.setOnTouchListener(touchListener);
        imgvIncrementLeft.setOnTouchListener(touchListener);
        imgvDecrementGo.setOnTouchListener(touchListener);
        imgvDecrementRight.setOnTouchListener(touchListener);
        imgvDecrementLeft.setOnTouchListener(touchListener);

        imgvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RobotManager manager = RobotManager.getInstance(CustomizeRobotActivity.this);
                robot.setName(robotNameEditText.getText().toString());

                manager.saveRobot(robot);
                onBackPressed();
            }
        });

    }

    protected void setDelayValuesForTextViews(){
        txtvGoDelay.setText("" + robot.getGoDelay());
        txtvLeftDelay.setText("" + robot.getLeftDelay());
        txtvRightDelay.setText("" + robot.getRightDelay());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customize_robot);

        robotCommunicator = new Communicator();
        robotCommunicator.listener = this;

        setupUI();
        setupListeners();
        setupActionBar();
        Bundle bundle = getIntent().getExtras();
        String robotAddress = bundle.getString(BundleConstants.DEVICE_ADDRESS);
        Robot r = (RobotManager.getInstance(getApplicationContext())).getRobotWithMacAdress(robotAddress);



        if (r == null){
            r = new Robot();
            r.setMacAdress(robotAddress);
            r.setAvatarPath("s");
        }
        if (r.isBonded()){
            robotCommunicator.startBTCommunicator(robotAddress, getResources(), getApplicationContext());
        }
        setRobot(r);
        setDelayValuesForTextViews();
    }

    private void setRobot(Robot newRobot){

        robot = newRobot;

        avatarImageView.setImageBitmap(robot.getAvatar(getApplicationContext()));
        macAddressTextView.setText(robot.getMacAdress());
        robotNameEditText.setText(robot.getName());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case SELECT_PHOTO:
                if(resultCode == RESULT_OK && data != null){

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA };

                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    avatarImageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                    robot.setAvatarPath(picturePath);
                }
        }
    }

    private void setupActionBar(){
        setTitle("Dopasuj robota");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.customize_robot_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStop() {
        super.onStop();
        robotCommunicator.destroyBTCommunicator();
    }

    @Override
    public void refreshGridView() {
        return;
    }

    @Override
    public void presentMessage(final String message) {

        connectingProgressDialog = ProgressDialog.show(this, "", message);


    }

    @Override
    public void hideMessage() {
        if (connectingProgressDialog != null){
            connectingProgressDialog.dismiss();
        }

    }

    private boolean isButtonPressed;
    private static final int INCREMENT_LEFT = 0;
    private static final int INCREMENT_GO = 1;
    private static final int INCREMENT_RIGHT = 2;
    private static final int DECREMENT_LEFT = 3;
    private static final int DECREMENT_GO = 4;
    private static final int DECREMENT_RIGHT = 5;

    private void startIncrementThread(final int type){
        new Thread(){
            @Override
            public void run() {
//                try {
//                    Thread.sleep(500);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                int i = 700;
                while(isButtonPressed){
                    switch (type){
                        case INCREMENT_LEFT:
                            robot.setLeftDelay((long) (robot.getLeftDelay() +INCREMENT_DECREMENT_FACTOR));
                            break;
                        case INCREMENT_GO:
                            robot.setGoDelay((long) (robot.getGoDelay() +INCREMENT_DECREMENT_FACTOR));
                            break;
                        case INCREMENT_RIGHT:
                            robot.setRightDelay((long) (robot.getRightDelay() +INCREMENT_DECREMENT_FACTOR));
                            break;
                        case DECREMENT_LEFT:
                            if(robot.getLeftDelay()<1){
                                robot.setLeftDelay(0);
                            }else{
                                robot.setLeftDelay((long) (robot.getLeftDelay() -INCREMENT_DECREMENT_FACTOR));
                            }
                            break;
                        case DECREMENT_GO:
                            if(robot.getGoDelay()<1){
                                robot.setGoDelay(0);
                            }else{
                                robot.setGoDelay((long) (robot.getGoDelay() -INCREMENT_DECREMENT_FACTOR));
                            }
                            break;
                        case DECREMENT_RIGHT:
                            if(robot.getRightDelay()<1){
                                robot.setRightDelay(0);
                            }else{
                                robot.setRightDelay((long) (robot.getRightDelay() -INCREMENT_DECREMENT_FACTOR));
                            }

                            break;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setDelayValuesForTextViews();
                        }
                    });

                    try {
                        Thread.sleep(Math.max(i,10));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    i*=0.7;
                }
            }
        }.start();
    }

    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {



            switch (motionEvent.getAction()){

                case MotionEvent.ACTION_DOWN:
                    Log.d("MOVE", "Action: Down");
                    isButtonPressed = true;
                    int type = -1;
                    if(view.equals(imgvIncrementGo)){
                        type = INCREMENT_GO;
                        robot.setGoDelay(robot.getGoDelay() + INCREMENT_DECREMENT_FACTOR);
                    } else if(view.equals(imgvIncrementLeft)){
                        type = INCREMENT_LEFT;
                        robot.setLeftDelay(robot.getLeftDelay() + INCREMENT_DECREMENT_FACTOR);
                    } else if(view.equals(imgvIncrementRight)){
                        type = INCREMENT_RIGHT;
                        robot.setRightDelay(robot.getRightDelay() + INCREMENT_DECREMENT_FACTOR);
                    } else if(view.equals(imgvDecrementGo)){
                        type = DECREMENT_GO;
                        robot.setGoDelay(robot.getGoDelay() - INCREMENT_DECREMENT_FACTOR);
                    } else if(view.equals(imgvDecrementLeft)){
                        type = DECREMENT_LEFT;
                        robot.setLeftDelay(robot.getLeftDelay() - INCREMENT_DECREMENT_FACTOR);
                    } else if(view.equals(imgvDecrementRight)){
                        type = DECREMENT_RIGHT;
                        robot.setRightDelay(robot.getRightDelay() - INCREMENT_DECREMENT_FACTOR);
                    }

                    startIncrementThread(type);
                    break;
                case MotionEvent.ACTION_MOVE:
                    Log.d("MOVE", "Action: Move");
                    break;
                case MotionEvent.ACTION_UP:
                    Log.d("MOVE", "Action: Up");
                    isButtonPressed = false;
                    break;
            }
            return true;
        }
    };
}
