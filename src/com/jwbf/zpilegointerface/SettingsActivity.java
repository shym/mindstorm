package com.jwbf.zpilegointerface;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import com.jwbf.zpilegointerface.datamodel.Robot;
import com.jwbf.zpilegointerface.dialog.RobotArrayAdapter;


public class SettingsActivity extends AbstractRobotListActivity{


    protected void setupUi(){
        robotListView = (ListView) findViewById(R.id.allRobotsListView);
    }

    protected void handleOnClickRobot(Robot robot){
        Intent intent = new Intent(SettingsActivity.this, CustomizeRobotActivity.class);
        intent.putExtra(BundleConstants.DEVICE_ADDRESS, robot.getMacAdress());
        startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        setupActionBar();
        fillListWithNewData();
    }

    /* Action bar and Menu Items setup */

    private void setupActionBar(){
        ActionBar actionBar = getActionBar();

        setTitle("Zarządzaj robotami");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refreshList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected boolean shouldRefresh() {
        return false;
    }

    /**
     * Provides content for table view.
     */
    protected void fillListWithNewData(){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Robot[] robots = robotManager.getAllRobotsArray();

                System.out.println("All robots: " + robots.length);
                robotsArrayAdapter = new RobotArrayAdapter(SettingsActivity.this, R.layout.lvitem_robot, robots);
                robotListView.setAdapter(robotsArrayAdapter);
            }
        });

    }



}

