package com.jwbf.zpilegointerface;

/**
 * Created by jakub on 24.10.13.
 */
public class BundleConstants {

    // Intent extra
    public static final String DEVICE_NAME_AND_ADDRESS = "device_infos";
    public static final String DEVICE_ADDRESS = "device_address";
    public static final String PAIRING = "pairing";

}
