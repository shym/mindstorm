package com.jwbf.zpilegointerface;

/**
 * Created by jakub on 07.11.13.
 */
public class RobotManagerEvents {

    public static final int EVENT_ADDED_NEARBY_ROBOT = 0x01;
    public static final int EVENT_DISCOVERY_FINISHED = 0x02;
    public static final int EVENT_BONDING_FINISHED = 0x03;
    public static final int EVENT_ARROW_DROPPED = 0x04;
    public static final int EVENT_ARROW_REMOVED = 0x05;
    public static final int EVENT_STOP_MOTORS= 0x06;
    public static final int EVENT_DRAG_ENDED = 0x07;
    public static final int EVENT_DROPPED_ON_GRID_VIEW = 0x08;

}
