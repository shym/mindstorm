package com.jwbf.zpilegointerface;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import com.jwbf.zpilegointerface.datamodel.Robot;
import com.jwbf.zpilegointerface.datamodel.RobotManager;
import com.jwbf.zpilegointerface.dialog.RobotActionAdapter;
import com.jwbf.zpilegointerface.draganddrop.ArrowClickListener;
import com.jwbf.zpilegointerface.draganddrop.DragListener;
import com.jwbf.zpilegointerface.draganddrop.DragTouchListener;
import com.jwbf.zpilegointerface.lego.BTCommunicator;
import com.jwbf.zpilegointerface.lego.BTConnectable;
import com.jwbf.zpilegointerface.lego.Communicator;
import com.jwbf.zpilegointerface.lego.CommunicatorListener;
import com.jwbf.zpilegointerface.observer.Observable;
import com.jwbf.zpilegointerface.observer.Observer;

import java.util.ArrayList;

/**
 * Created by jakub on 24.10.13.
 */


public class GameActivity extends Activity implements BTConnectable, Observer, CommunicatorListener {

//    private boolean connected;

    private String macAddress;

    private ProgressDialog connectingProgressDialog;
    private Robot currentlyConnectedRobot;

    private ArrayList<RobotAction> robotActions = new ArrayList<RobotAction>();

    private ImageView imgvGo;
    private ImageView imgvTurnLeft;
    private ImageView imgvTurnRight;
    private ImageView imgvClear;
    private ImageView imgvStop;
    private ImageView imgvStart;

    private GridView gvRouteLog;
    private RobotActionAdapter robotActionAdapter;

    private RobotManager robotManager;

    private Communicator robotCommunicator;

    private void getExtrasFromIntent(){
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if(extras != null){

            // Get the device MAC address and start a new bt communicator thread
            macAddress = extras.getString(BundleConstants.DEVICE_ADDRESS);
            currentlyConnectedRobot = robotManager.getRobotByMac(macAddress);

            robotCommunicator.startBTCommunicator(macAddress, getResources(),getApplicationContext());
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_design);
        setupActionBar();
        robotCommunicator = new Communicator();
        robotCommunicator.listener = this;
        robotManager = RobotManager.getInstance(getApplicationContext());
        getExtrasFromIntent();
        imgvGo = (ImageView) findViewById(R.id.imgvGo);
        imgvGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                robotActions.add(RobotAction.actionGo(currentlyConnectedRobot));
                populateGridViewWithData();
            }
        });
        imgvGo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                robotCommunicator.playAction(RobotAction.actionGo(currentlyConnectedRobot));
                return false;
            }
        });

        imgvGo.setOnTouchListener(new DragTouchListener(RobotAction.ACTION_TYPE_GO));

        imgvTurnLeft = (ImageView) findViewById(R.id.imgvTurnLeft);
        imgvTurnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                robotActions.add(RobotAction.actionLeft(currentlyConnectedRobot));
                populateGridViewWithData();
            }
        });
        imgvTurnLeft.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                robotCommunicator.playAction(RobotAction.actionLeft(currentlyConnectedRobot));
                return false;
            }
        });

        imgvTurnLeft.setOnTouchListener(new DragTouchListener(RobotAction.ACTION_TYPE_LEFT));

        imgvTurnRight = (ImageView) findViewById(R.id.imgvTurnRight);
        imgvTurnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                robotActions.add(RobotAction.actionRight(currentlyConnectedRobot));
                populateGridViewWithData();
            }
        });
        imgvTurnRight.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                robotCommunicator.playAction(RobotAction.actionRight(currentlyConnectedRobot));
                return false;
            }
        });

        imgvTurnRight.setOnTouchListener(new DragTouchListener(RobotAction.ACTION_TYPE_RIGHT));

        imgvClear = (ImageView) findViewById(R.id.imgvRouteClear);
        imgvClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearGridView();
            }
        });

        imgvStop = (ImageView) findViewById(R.id.imgvRouteStop);
        imgvStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                robotCommunicator.stop = true;
                robotCommunicator.myBTCommunicator.changeMotorSpeed(BTCommunicator.MOTOR_B,0);
                robotCommunicator.myBTCommunicator.changeMotorSpeed(BTCommunicator.MOTOR_C,0);
                Log.d("e", "stopped");
//                robotCommunicator.playAction(RobotAction.actionStop());
            }
        });

        imgvStart = (ImageView) findViewById(R.id.imgvRouteStart);
        imgvStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!robotCommunicator.isPlaying){
                    if (robotCommunicator.myBTCommunicator == null){
                        robotCommunicator.startBTCommunicator(currentlyConnectedRobot.getMacAdress(),getResources(), getApplicationContext());
                    }
                    else{
                        robotCommunicator.playActions(robotActions);
                    }

                }
            }
        });


        gvRouteLog = (GridView) findViewById(R.id.gvRouteLog);
        gvRouteLog.setOnDragListener(new DragListener(getApplicationContext()));

        gvRouteLog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });
    }



    private void populateGridViewWithData(){
        RobotAction[] actions = new RobotAction[robotActions.size()];
        actions = robotActions.toArray(actions);

        robotActionAdapter = new RobotActionAdapter(getApplicationContext(), actions);
        robotActionAdapter.setArrowClickListener(new ArrowClickListener() {
            @Override
            public void clickedOnArrow(int id) {
                deselectAll(id);
                populateGridViewWithData();
            }
        });
        gvRouteLog.setAdapter(robotActionAdapter);
    }

    private void deselectAll(int idClicked){
        RobotAction selectedAction = robotActions.get(idClicked);
        boolean wasSelected = selectedAction.isActive();

        for(RobotAction action : robotActions){
            action.setActive(false);
        }

        if(!wasSelected){
            selectedAction.setActive(true);
            robotActions.set(idClicked, selectedAction);
        }
    }

    private void handleArrowDroppedOnGridView(){
        int movedArrowId = robotManager.getArrowIdToRemove();
        DragEvent dragEvent = robotManager.getLastDragEvent();

        int columnWidth = gvRouteLog.getColumnWidth();
        int rowHeight = columnWidth;

        float x = dragEvent.getX();
        float y = dragEvent.getY();

        float columnPercentage = x/gvRouteLog.getWidth();
        int columnsCount = gvRouteLog.getWidth() / columnWidth;

        float rowPercentage = y/gvRouteLog.getHeight();
        int rowCount = gvRouteLog.getHeight() / rowHeight;


        RobotAction actionToMove = robotActions.remove(movedArrowId);

        int column = (int)(columnPercentage * columnsCount);
        int row = (int)(rowPercentage * rowCount);

        int position = (column + row * columnsCount);

        if (position >= robotActions.size()){
            robotActions.add(actionToMove);
        }
        else{
            robotActions.add(position, actionToMove);
        }
        populateGridViewWithData();
    }

    private void handleOnArrowDropped(int type){
        switch (type){
            case RobotAction.ACTION_TYPE_LEFT:
                robotActions.add(RobotAction.actionLeft(currentlyConnectedRobot));
                break;
            case RobotAction.ACTION_TYPE_RIGHT:
                robotActions.add(RobotAction.actionRight(currentlyConnectedRobot));
                break;
            case RobotAction.ACTION_TYPE_GO:
                robotActions.add(RobotAction.actionGo(currentlyConnectedRobot));
                break;
        }

        populateGridViewWithData();
    }

    private void handleOnArrowRemoved(int id){
        robotActions.remove(id);
        populateGridViewWithData();
    }

    @Override
    protected void onStop() {
        super.onStop();
        robotManager.delObserver(this);
        robotCommunicator.destroyBTCommunicator();
    }

    @Override
    protected void onStart() {
        super.onStart();
        robotManager.addObserver(this);
    }

    @Override
    public boolean isPairing() {
        return false;
    }

    private void setupActionBar(){
        ActionBar actionBar = getActionBar();
        setTitle("Ułóż trasę");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.communication_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public void update(Observable o, int event) {
        switch (event){
            case RobotManagerEvents.EVENT_ARROW_DROPPED:
                handleOnArrowDropped(robotManager.getArrowTypeOnDrop());
                break;
            case RobotManagerEvents.EVENT_ARROW_REMOVED:
                handleOnArrowRemoved(robotManager.getArrowIdToRemove());
                break;
            case RobotManagerEvents.EVENT_STOP_MOTORS:
                robotCommunicator.playAction(RobotAction.actionStop());
                break;
            case RobotManagerEvents.EVENT_DRAG_ENDED:
                populateGridViewWithData();
                break;
            case RobotManagerEvents.EVENT_DROPPED_ON_GRID_VIEW:
                handleArrowDroppedOnGridView();
                break;

        }
        //gvRouteLog.invalidate();
    }

    @Override
    public void refreshGridView() {
       runOnUiThread(new Runnable() {
            @Override
            public void run() {
                populateGridViewWithData();
            }
       });
    }

    @Override
    public void presentMessage(final String message) {

        connectingProgressDialog = ProgressDialog.show(this,"",message);


    }

    @Override
    public void hideMessage() {
        if (connectingProgressDialog != null){
            connectingProgressDialog.dismiss();
        }

    }

    private void clearGridView(){
        robotActions = new ArrayList<RobotAction>();
        populateGridViewWithData();

    }
}
