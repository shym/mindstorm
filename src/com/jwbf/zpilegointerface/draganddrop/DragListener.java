package com.jwbf.zpilegointerface.draganddrop;

import android.content.ClipData;
import android.content.Context;
import android.view.DragEvent;
import android.view.View;
import com.jwbf.zpilegointerface.RobotManagerEvents;
import com.jwbf.zpilegointerface.datamodel.RobotManager;

/**
 * Created by jakub on 20.11.13.
 */
public class DragListener implements View.OnDragListener{

    private RobotManager robotManager;

    private boolean removeItem;
    private int idToRemove = -1;

    public DragListener(Context context){
        robotManager = RobotManager.getInstance(context);
    }

    @Override
    public boolean onDrag(View view, DragEvent dragEvent) {
        String dragCase = "fail";
        try{
            dragCase = String.valueOf(dragEvent.getClipDescription().getLabel());
        } catch (NullPointerException e){

        }

        if(removeItem){
            removeItem = false;
            robotManager.notifyObservers(RobotManagerEvents.EVENT_ARROW_REMOVED);
        }

        switch (dragEvent.getAction()){


            case DragEvent.ACTION_DRAG_EXITED:
                if(dragCase.equals("id")){
                    removeItem = true;
                }
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                if(dragCase.equals("id")){
                    removeItem = false;
                }
                break;
            case DragEvent.ACTION_DROP:
                ClipData data = dragEvent.getClipData();
                ClipData.Item item = data.getItemAt(0);

                if(dragCase.equals("type")){
                    int type = Integer.valueOf(item.getText() + "");
                    robotManager.setArrowTypeOnDrop(type);
                    robotManager.notifyObservers(RobotManagerEvents.EVENT_ARROW_DROPPED);
                } else {
                    int id = Integer.valueOf(item.getText() + "");
                    if(removeItem){
                        robotManager.setArrowIdToRemove(id);
                        robotManager.notifyObservers(RobotManagerEvents.EVENT_ARROW_REMOVED);
                        removeItem = false;
                    } else {
                        view.setVisibility(View.VISIBLE);
                        //TODO: KURWA
                        robotManager.setLastDragEvent(dragEvent);
                        robotManager.notifyObservers(RobotManagerEvents.EVENT_DROPPED_ON_GRID_VIEW);
                    }
                }
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                robotManager.notifyObservers(RobotManagerEvents.EVENT_DRAG_ENDED);
                break;
        }

        return true;
    }
}
