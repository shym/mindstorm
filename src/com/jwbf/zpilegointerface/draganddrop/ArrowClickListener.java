package com.jwbf.zpilegointerface.draganddrop;

/**
 * Created by jakub on 05.12.13.
 */
public interface ArrowClickListener {
    public void clickedOnArrow(int id);
}
