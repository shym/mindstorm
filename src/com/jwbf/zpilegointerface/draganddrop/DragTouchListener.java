package com.jwbf.zpilegointerface.draganddrop;

import android.content.ClipData;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.jwbf.zpilegointerface.datamodel.RobotManager;

/**
 * Created by jakub on 20.11.13.
 */
public class DragTouchListener implements View.OnTouchListener{

    private int type;
    private boolean remove;
    private int movedPixels;

    private static final int PIXELS_TO_MOVE = 20;

    private int lastX;
    private int lastY;

    private ArrowClickListener arrowClickListener;

    public DragTouchListener(int type){
        this.type = type;
        this.remove = false;
    }

    public DragTouchListener(int id, boolean remove){
        this.type = id;
        this.remove = remove;

    }

    public void setArrowClickListener(ArrowClickListener arrowClickListener) {
        this.arrowClickListener = arrowClickListener;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        Log.d("DRAG", "onTouch !");
        if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
            int newX = (int)motionEvent.getX();
            int newY = (int)motionEvent.getY();

            movedPixels += Math.abs(newX - lastX);
            movedPixels += Math.abs(newY - lastY);

            lastX = newX;
            lastY = newY;

            Log.d("MOVE", "Moved pixels: " + movedPixels);

            if(movedPixels >= PIXELS_TO_MOVE){
                if(!remove){
                    handleAdd(view);
                } else {
                    handleRemove(view);
                }
            }
//            view.setVisibility(View.INVISIBLE);
            return false;
        } else if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
//            if(remove){
//                handleRemove(view);
//            }

            lastX = (int)motionEvent.getX();
            lastY = (int)motionEvent.getY();
            return true;
        }
        else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
            if(movedPixels < PIXELS_TO_MOVE){
                Log.d("MOVE", "Clicked");
                if(arrowClickListener != null && remove){
                    arrowClickListener.clickedOnArrow(type);
                }
                view.performClick();
                return true;
            }
            movedPixels = 0;
            return false;
        }
        else {
            return true;
        }
    }

    private void handleAdd(View view){
        ClipData data = ClipData.newPlainText("type", type + "");
        ClipData.Item item = new ClipData.Item(type + "");
        data.addItem(item);
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
        view.startDrag(data, shadowBuilder, view, 0);
    }

    private void handleRemove(View view){
        ClipData data = ClipData.newPlainText("id", type + "");
        ClipData.Item item = new ClipData.Item(type + "");
        data.addItem(item);
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
        view.startDrag(data, shadowBuilder, view, 0);
        view.setVisibility(View.INVISIBLE);
        RobotManager.getInstance(null).setArrowIdToRemove(type);
    }
}
