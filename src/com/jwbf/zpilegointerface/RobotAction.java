package com.jwbf.zpilegointerface;

import com.jwbf.zpilegointerface.datamodel.Robot;

/**
 * Created by tetek on 11/14/13.
 */


public class RobotAction {

    private int leftMotorID = com.lego.minddroid.BTCommunicator.MOTOR_C;
    private int leftDelay = 1250;
    private int leftMotorPower = 65;

    private int rightMotorID = com.lego.minddroid.BTCommunicator.MOTOR_B;
    private int rightDelay = leftDelay;
    private int rightMotorPower = 65;

    private int actionType;
    private boolean isActive;
    public static final int ACTION_TYPE_LEFT = 0;
    public static final int ACTION_TYPE_RIGHT = 1;
    public static final int ACTION_TYPE_GO = 2;
    public static final int ACTION_TYPE_STOP = 3;

    public int getLeftMotorID() {
        return leftMotorID;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public int getLeftDelay() {
        return leftDelay;
    }

    public int getLeftMotorPower() {
        return leftMotorPower;
    }

    public int getRightMotorID() {
        return rightMotorID;
    }

    public int getRightDelay() {
        return rightDelay;
    }

    public int getRightMotorPower() {
        return rightMotorPower;
    }

    public int getActionType() {
        return actionType;
    }

    public RobotAction(Robot robot){
        rightDelay = (int)robot.getRightDelay();
        leftDelay = (int)robot.getLeftDelay();
    }

    public RobotAction(){}

    public static RobotAction actionLeft(Robot robot){

        RobotAction r = new RobotAction(robot);
        r.rightDelay = (int)robot.getLeftDelay();
        r.leftDelay = (int)robot.getLeftDelay();
        r.leftMotorPower *= -1;
        r.actionType = ACTION_TYPE_LEFT;

        return r;
    }

    public static RobotAction actionRight(Robot robot){

        RobotAction r = new RobotAction(robot);
        r.rightDelay = (int)robot.getRightDelay();
        r.leftDelay = (int)robot.getRightDelay();
        r.rightMotorPower *= -1;
        r.actionType = ACTION_TYPE_RIGHT;

        return r;
    }

    public static RobotAction actionGo(Robot robot){

        RobotAction r = new RobotAction(robot);
        r.actionType = ACTION_TYPE_GO;
        r.leftDelay = r.rightDelay = (int)robot.getGoDelay();

        return r;
    }
    public static RobotAction actionStop(){

        RobotAction r = new RobotAction();
        r.actionType = ACTION_TYPE_STOP;
        r.leftMotorPower = 0;
        r.leftDelay = 0;
        r.rightDelay = 0;
        r.rightMotorPower = 0;
        return r;
    }
}
