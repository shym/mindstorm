package com.jwbf.zpilegointerface.observer;

/**
 * Created by jakub on 07.11.13.
 */
public interface Observer {
    public void update(Observable o, int event);
}
