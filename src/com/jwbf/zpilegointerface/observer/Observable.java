package com.jwbf.zpilegointerface.observer;

/**
 * Created by jakub on 07.11.13.
 */
public interface Observable {
    public void addObserver(Observer o);
    public void delObserver(Observer o);
}
