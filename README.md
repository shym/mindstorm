# Lego Mindstorm Android Controller #

This is an android app communicating with lego mindstorm via bluetooth. The app interface is child-friendly.

### What is this repository for? ###

* Experimenting with lego mindstorm
* Lego bluetooth communicator protocol
* Teaching kids to plan routes for robots.

### How do I get set up? ###

* checkout project
* open via Android Studio


### Who do I talk to? ###

* Repo owner or admin
* tetek or shym